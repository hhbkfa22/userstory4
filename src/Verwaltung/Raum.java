package Verwaltung;

public class Raum {
    private String id;
    private String typ;
    private double groesse;

    public Raum(
            String id,
            String typ,
            double groesse
    ) {
        this.id = id;
        this.typ = typ;
        this.groesse = groesse;
    }

    public String getId() {
        return id;
    }

    public String getTyp() {
        return typ;
    }

    public double getGroesse() {
        return groesse;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public void setGroesse(double groesse) {
        this.groesse = groesse;
    }

    @Override
    public String toString() {
        return getId() +
                ";" + getTyp() +
                ";" + getGroesse();
    }
}
