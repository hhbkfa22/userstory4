package Verwaltung;

import javafx.scene.control.Alert;

import java.time.LocalDate;

public class Hardware {
    private int id;
    private String serienNummer;
    private String modell;
    private String hersteller;
    private String status;
    private int garantie;
    private LocalDate lieferDatum;
    private Raum raum;

    public Hardware() {
    }

    public Hardware(String serienNummer,
                    String modell,
                    String hersteller,
                    String status,
                    int garantie,
                    LocalDate lieferDatum,
                    Raum raum
    ) {
        this.serienNummer = serienNummer;
        this.modell = modell;
        this.hersteller = hersteller;
        this.status = status;
        this.garantie = garantie;
        this.lieferDatum = lieferDatum;
        this.raum = raum;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String getSerienNummer() {
        return serienNummer;
    }

    public String getModell() {
        return modell;
    }

    public String getHersteller() {
        return hersteller;
    }

    public String getStatus() {
        return status;
    }

    public int getGarantie() {
        return garantie;
    }

    public LocalDate berechneGarantieEnde()
    {
        return lieferDatum.plusMonths(garantie);
    }

    public LocalDate getLieferDatum() {
        return lieferDatum;
    }

    public void setSerienNummer(String serienNummer) {
        this.serienNummer = serienNummer;
    }

    public void setModell(String modell) {
        this.modell = modell;
    }

    public void setHersteller(String hersteller) {
        this.hersteller = hersteller;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setGarantie(int garantie) {
        this.garantie = garantie;
    }

    public void setLieferDatum(LocalDate lieferDatum) {
        this.lieferDatum = lieferDatum;
    }

    public Raum getRaum() {
        return raum;
    }

    public void setRaum(Raum raum) {
        this.raum = raum;
    }

    public void createAlert(Alert.AlertType alertType, String alertMessage)
    {
        Alert alert = new Alert(alertType, alertMessage);
        alert.showAndWait();
    }
}
