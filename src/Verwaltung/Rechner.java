package Verwaltung;

import java.time.LocalDate;

public class Rechner extends Hardware {
    public static int anzahl;

    private String cpu;
    private int ram;
    private String os;
    private String typ;
    private String gpu;
    private int ssd;
    private int hdd;

    public Rechner(String serienNummer,
                   String modell,
                   String hersteller,
                   String status,
                   int herstellerGarantie,
                   LocalDate lieferDatum,
                   String cpu,
                   int ram,
                   String os,
                   String typ,
                   String gpu,
                   int ssd,
                   int hdd,
                   Raum raum
    ){
        super(serienNummer, modell, hersteller, status, herstellerGarantie, lieferDatum, raum);
        this.setId();
        this.cpu = cpu;
        this.ram = ram;
        this.os = os;
        this.typ = typ;
        this.gpu = gpu;
        this.ssd = ssd;
        this.hdd = hdd;
    }

    public void setId() {
        anzahl++;
        super.setId(anzahl);
    }

    public String getCpu() {
        return cpu;
    }

    public void setCpu(String cpu) {
        this.cpu = cpu;
    }

    public int getRam() {
        return ram;
    }

    public void setRam(int ram) {
        this.ram = ram;
    }

    public String getOs() {
        return os;
    }

    public void setOs(String os) {
        this.os = os;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getGpu() {
        return gpu;
    }

    public void setGpu(String gpu) {
        this.gpu = gpu;
    }

    public int getSsd() {
        return ssd;
    }

    public void setSsd(int ssd) {
        this.ssd = ssd;
    }

    public int getHdd() {
        return hdd;
    }

    public void setHdd(int hdd) {
        this.hdd = hdd;
    }

    @Override
    public String toString() {
        return super.getId() +
                ";" + super.getSerienNummer() +
                ";" + super.getModell() +
                ";" + super.getHersteller() +
                ";" + super.getStatus() +
                ";" + super.getGarantie() +
                ";" + super.getLieferDatum() +
                ";" + this.cpu +
                ";" + this.ram +
                ";" + this.os +
                ";" + this.typ +
                ";" + this.gpu +
                ";" + this.ssd +
                ";" + this.hdd +
                ";" + this.getRaum().getId();
    }
}
