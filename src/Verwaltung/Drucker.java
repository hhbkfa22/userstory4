package Verwaltung;

import javafx.scene.control.Alert;

import java.time.LocalDate;

public class Drucker extends Hardware {
    public static int anzahl;

    private String technologie;
    private boolean farbDruckFunktion;
    private String papierFormatMax;
    private int druckSeitenGesamt;
    private int restKapazitaet = 200;
    private int kapazitaetBetriebsmittel = 200;

    public Drucker(String serienNummer,
                   String modell,
                   String hersteller,
                   String status,
                   int herstellerGarantie,
                   LocalDate lieferDatum,
                   String technologie,
                   boolean farbDruckFunktion,
                   String papierFormatMax,
                   Raum raum
    ){
        super(serienNummer, modell, hersteller, status, herstellerGarantie, lieferDatum, raum);
        this.setId();
        this.technologie = technologie;
        this.farbDruckFunktion = farbDruckFunktion;
        this.papierFormatMax = papierFormatMax;
        this.kapazitaetBetriebsmittel = 200;
        this.restKapazitaet = 200;
    }

    public void setId() {
        anzahl++;
        super.setId(anzahl);
    }

    public String getTechnologie() {
        return technologie;
    }

    public void setTechnologie(String technologie) {
        this.technologie = technologie;
    }

    public boolean isFarbDruckFunktion() {
        return farbDruckFunktion;
    }

    public void setFarbDruckFunktion(boolean farbDruckFunktion) {
        this.farbDruckFunktion = farbDruckFunktion;
    }

    public String getPapierFormatMax() {
        return papierFormatMax;
    }

    public void setPapierFormatMax(String papierFormatMax) {
        this.papierFormatMax = papierFormatMax;
    }

    public int getDruckSeitenGesamt() {
        return druckSeitenGesamt;
    }

    public void setDruckSeitenGesamt(int druckSeitenGesamt) {
        this.druckSeitenGesamt = druckSeitenGesamt;
    }

    public int getRestKapazitaet() {
        return restKapazitaet;
    }

    public void setRestKapazitaet(int restKapazitaet) {
        this.restKapazitaet = restKapazitaet;
    }

    public int getKapazitaetBetriebsmittel() {
        return kapazitaetBetriebsmittel;
    }

    public void setKapazitaetBetriebsmittel(int kapazitaetBetriebsmittel) {
        this.kapazitaetBetriebsmittel = kapazitaetBetriebsmittel;
    }

    @Override
    public String toString() {
        return super.getId() +
                ";" + super.getSerienNummer() +
                ";" + super.getModell() +
                ";" + super.getHersteller() +
                ";" + super.getStatus() +
                ";" + super.getGarantie() +
                ";" + super.getLieferDatum() +
                ";" + this.technologie +
                ";" + this.farbDruckFunktion +
                ";" + this.papierFormatMax +
                ";" + this.druckSeitenGesamt +
                ";" + this.restKapazitaet +
                ";" + this.kapazitaetBetriebsmittel +
                ";" + this.getRaum().getId();
    }

    public void wechsleBetriebsmittel(int kapazitaet)
    {
        if (kapazitaet < 1) {
            this.createAlert(Alert.AlertType.ERROR, "Die Kapazität muss größer als 0 sein!");
        } else {
            this.kapazitaetBetriebsmittel = kapazitaet;
            this.restKapazitaet = kapazitaet;
        }
    }

    public void drucken(int anzahlSeiten)
    {
        if (anzahlSeiten < 0) {
            this.createAlert(Alert.AlertType.WARNING, "Anzahl Seiten muss größer 0 sein!");
        } else {
            if (this.restKapazitaet > anzahlSeiten) {
                this.druckSeitenGesamt += anzahlSeiten;
                this.restKapazitaet -= anzahlSeiten;
                this.createAlert(Alert.AlertType.INFORMATION, anzahlSeiten + " Seiten wurden erfolgreich gedruckt!");
            } else {
                int fehlendeSeiten = anzahlSeiten - this.restKapazitaet;
                int gedruckteSeiten = anzahlSeiten - fehlendeSeiten;
                this.druckSeitenGesamt += gedruckteSeiten;
                this.createAlert(Alert.AlertType.WARNING, "Es konnten nur "
                        + gedruckteSeiten
                        + " Seiten erfolgreich gedruckt werden!"
                        + '\n'
                        + "Bitte wechseln Sie das Betriebsmittel!");
            }
        }
    }
}
