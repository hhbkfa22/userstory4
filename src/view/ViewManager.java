package view;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class ViewManager
{
	
	private static ViewManager viewmanager = null;
	
	private Scene scene_rechner = null;
	private Scene scene_drucker = null;
	private Scene scene_dashboard = null;
	private Scene scene_raum = null;
	private Pane pane_dashboard = null;
	private Pane pane_rechner = null;
	private Pane pane_drucker = null;
	private Pane pane_raum = null;
	
	private Stage primarystage = null;
	
	private ViewManager()
	{
		try
		{
			this.pane_rechner =
					FXMLLoader.load(getClass()
											.getResource("Rechnerverwaltung.fxml"));
			this.pane_drucker =
					FXMLLoader.load(getClass()
											.getResource("Druckerverwaltung.fxml"));
			this.pane_dashboard =
					FXMLLoader.load(getClass()
											.getResource("ViewDashboard.fxml"));
			this.pane_raum =
					FXMLLoader.load(getClass()
											.getResource("Raumverwaltung.fxml"));
			
			this.scene_rechner = new Scene(pane_rechner);
			this.scene_drucker = new Scene(pane_drucker);
			this.scene_dashboard = new Scene(pane_dashboard);
			this.scene_raum = new Scene(pane_raum);
		}
		catch (Exception e)
		{
			Alert alert = new Alert(Alert.AlertType.ERROR,
									e.getMessage());
			alert.showAndWait();
		}
	}
	
	public static ViewManager getInstance()
	{
		if (ViewManager.viewmanager == null)
		{
			ViewManager.viewmanager = new ViewManager();
		}
		return ViewManager.viewmanager;
	}
	
	/**
	 * @param scene
	 */
	public void activateScene(Scene scene)
	{
		this.primarystage.setScene(scene);
		this.primarystage.show();
	}
	
	/**
	 * @param primaryStage
	 */
	public void setStage(Stage primaryStage)
	{
		this.primarystage = primaryStage;
	}
	
	
	public Scene getScene_rechner()
	{
		return this.scene_rechner;
	}
	
	public Scene getScene_drucker()
	{
		return this.scene_drucker;
	}
	
	public Scene getDashboardscene()
	{
		return this.scene_dashboard;
	}

	public Scene getScene_raum()
	{
		return this.scene_raum;
	}
}
