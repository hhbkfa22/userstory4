package view;

import Dao.DaoManager;
import javafx.application.Application;
import javafx.stage.Stage;

public class Start extends Application
{

    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage)
    {
        ViewManager.getInstance()
                   .setStage(primaryStage);

        primaryStage.setTitle("Hardwareverwaltung 03");

        ViewManager.getInstance()
                   .activateScene(ViewManager.getInstance()
                                             .getDashboardscene());

        primaryStage.setResizable(false);
    }
}
