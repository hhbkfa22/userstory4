package view;

import Dao.DaoManager;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import Verwaltung.Raum;
import java.net.URL;
import java.util.ResourceBundle;

public class ControllerRaum implements Initializable {
    @FXML
    private ListView<Raum> lv_Objects;
    private ObservableList<String> typListe = FXCollections.observableArrayList(
            "Klassenraum",
            "IT Fachraum",
            "ET Fachraum",
            "CH Labor",
            "Serviceraum",
            "Lehrervorbereitungsraum",
            "Büro",
            "Sonstiges"
    );
    private ObservableList<Raum> raumListe = FXCollections.observableArrayList();
    @FXML
    private TextField tf_Id;
    @FXML
    private ChoiceBox<String> cb_Typ;
    @FXML
    private TextField tf_Groesse;

    private Raum raum;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        var daoManager =DaoManager.getInstance();
        raumListe = daoManager.getListe_raum();
        this.cb_Typ.setItems(typListe);
        this.lv_Objects.setItems(raumListe);
    }

    @FXML
    private void handleBtnDashboard(ActionEvent e)
    {
        ViewManager.getInstance()
                .activateScene(ViewManager.getInstance()
                        .getDashboardscene());
    }

    @FXML
    private void handleBtnHardware(ActionEvent e)
    {
        ViewManager.getInstance()
                .activateScene(ViewManager.getInstance()
                        .getScene_rechner());
    }

    public void save() {
        try {
            String id = this.tf_Id.getText();
            String typ = this.cb_Typ.getValue();
            double groesse = Double.parseDouble(this.tf_Groesse.getText());

            if (this.raum == null) {
                raum = new Raum(id, typ, groesse);

                this.raumListe.add(raum);
            } else {
                raum.setId(id);
                raum.setTyp(typ);
                raum.setGroesse(groesse);

                this.raumListe.set(lv_Objects.getSelectionModel().getSelectedIndex(), null);
                this.raumListe.set(lv_Objects.getSelectionModel().getSelectedIndex(), raum);
                this.raum = null;
            }
        } catch (NumberFormatException e) {
            error("Jedes Textfeld muss mit einem Wert gefüllt sein!");
        } catch (Exception e) {
            error("Ein Fehler beim Speichern ist aufgetreten. Versuchen Sie es bitte erneut!");
        }
    }

    public void loadRaum(Event event)
    {
        try {
            this.raum = raumListe.get(lv_Objects.getSelectionModel().getSelectedIndex());
            this.tf_Id.setText(raum.getId());
            this.cb_Typ.setValue(raum.getTyp());
            this.tf_Groesse.setText(Double.toString(raum.getGroesse()));
        } catch (Exception e){
        }
    }

    public void reset() {
        this.tf_Id.setText("");
        this.cb_Typ.setValue("");
        this.tf_Groesse.setText("");
    }

    public void error(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Fehler beim Speichern");
        alert.setContentText(message);

        alert.showAndWait();
    }
}
