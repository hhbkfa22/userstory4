package view;

import Dao.DaoManager;
import Verwaltung.Drucker;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import Verwaltung.Raum;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class ControllerDrucker implements Initializable {
    public static int anzahl;
    @FXML
    private ListView<Drucker> lv_Objects;

    private ObservableList<String> statusListe = FXCollections.observableArrayList("ok", "in Reperatur", "defekt");
    private ObservableList<String> technologieListe = FXCollections.observableArrayList(
            "Farbtintenstrahldrucker",
            "Tintenstrahldrucker",
            "Farblaserdrucker",
            "Laserdrucker"
    );
    private ObservableList<Drucker> druckerListe = FXCollections.observableArrayList();
    private ObservableList<String> papierFormatList = FXCollections.observableArrayList("A3", "A4");
    private ObservableList<Raum> raumListe = FXCollections.observableArrayList();
    @FXML
    private TextField tf_Id;
    @FXML
    private DatePicker dp_Lieferdatum;
    @FXML
    private CheckBox cb_Farbdruck;
    @FXML
    private ChoiceBox<String> cb_Status;
    @FXML
    private ChoiceBox<String> cb_Technologie;
    @FXML
    private ChoiceBox<String> cb_Format;
    @FXML
    private ChoiceBox<Raum> cb_Raum;
    @FXML
    private TextField tf_Modell;
    @FXML
    private TextField tf_Hersteller;
    @FXML
    private TextField tf_Garantie;
    @FXML
    private TextField tf_Gedruckt;
    @FXML
    private TextField tf_Restkapazitaet;
    @FXML
    private TextField tf_Kapazitaet;
    @FXML
    private TextField tf_Seriennummer;

    private Drucker drucker;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        var daoManager = DaoManager.getInstance();
        druckerListe = daoManager.getListe_drucker();
        raumListe = daoManager.getListe_raum();
        anzahl+=druckerListe.size();
        anzahl++;
        this.tf_Id.setText(Integer.toString(anzahl));
        this.cb_Status.setValue("ok");
        this.cb_Status.setItems(this.statusListe);
        this.cb_Technologie.setValue("Farbtintenstrahldrucker");
        this.cb_Technologie.setItems(this.technologieListe);
        this.cb_Format.setValue("A4");
        this.cb_Format.setItems(this.papierFormatList);
        this.cb_Raum.setItems(raumListe);
        this.cb_Raum.setValue(raumListe.get(0));
        this.tf_Gedruckt.setText("0");
        this.tf_Kapazitaet.setText("200");
        this.tf_Restkapazitaet.setText("200");
        this.lv_Objects.setItems(this.druckerListe);
    }

    @FXML
    private void handleBtnDashboard(ActionEvent e)
    {
        ViewManager
                .getInstance()
                .activateScene(
                        ViewManager.
                                getInstance()
                                .getDashboardscene());
    }

    public void save() {
        try {
            String serienNummer = tf_Seriennummer.getText();
            String modell = tf_Modell.getText();
            String hersteller = tf_Hersteller.getText();
            String status = cb_Status.getValue();
            int garantie = Integer.parseInt(tf_Garantie.getText());
            LocalDate lieferDatum = dp_Lieferdatum.getValue();
            boolean farbDruck = cb_Farbdruck.isSelected();
            String technologie = cb_Technologie.getValue();
            String papierFormat = cb_Format.getValue();
            Raum raum = cb_Raum.getValue();

            if (this.drucker == null) {
                Drucker drucker = new Drucker(serienNummer, modell, hersteller, status, garantie, lieferDatum, technologie, farbDruck, papierFormat, raum);
                this.druckerListe.add(drucker);
                anzahl++;
                this.tf_Id.setText(Integer.toString(anzahl));
                this.drucker = null;
                this.reset();
            } else {
                this.drucker.setSerienNummer(serienNummer);
                this.drucker.setModell(modell);
                this.drucker.setHersteller(hersteller);
                this.drucker.setStatus(status);
                this.drucker.setGarantie(garantie);
                this.drucker.setLieferDatum(lieferDatum);
                this.drucker.setFarbDruckFunktion(farbDruck);
                this.drucker.setTechnologie(technologie);
                this.drucker.setPapierFormatMax(papierFormat);
                this.drucker.setRaum(raum);

                this.druckerListe.set(lv_Objects.getSelectionModel().getSelectedIndex(), null);
                this.druckerListe.set(lv_Objects.getSelectionModel().getSelectedIndex(), this.drucker);
                this.drucker = null;
            }
        } catch (NumberFormatException e) {
            error("Jedes Textfeld muss mit einem Wert gefüllt sein!");
        } catch (Exception e) {
            error("Ein Fehler beim Speichern ist aufgetreten. Versuchen Sie es bitte erneut!");
        }
    }

    public void reset() {
        this.tf_Id.setText(Integer.toString(anzahl));
        this.tf_Seriennummer.setText("");
        this.tf_Modell.setText("");
        this.tf_Hersteller.setText("");
        this.cb_Status.setValue("ok");
        this.tf_Garantie.setText("");
        this.dp_Lieferdatum.setValue(null);
        this.cb_Farbdruck.setSelected(false);
        this.cb_Technologie.setValue("Farbtintenstrahldrucker");
        this.cb_Format.setValue("A4");
    }

    public void loadDrucker() {
        try {
            this.drucker = druckerListe.get(lv_Objects.getSelectionModel().getSelectedIndex());
            this.tf_Id.setText(Integer.toString(this.drucker.getId()));
            this.tf_Seriennummer.setText(this.drucker.getSerienNummer());
            this.tf_Modell.setText(this.drucker.getModell());
            this.tf_Hersteller.setText(this.drucker.getHersteller());
            this.cb_Status.setValue(this.drucker.getStatus());
            this.tf_Garantie.setText(Integer.toString(this.drucker.getGarantie()));
            this.dp_Lieferdatum.setValue(this.drucker.getLieferDatum());
            this.cb_Farbdruck.setSelected(this.drucker.isFarbDruckFunktion());
            this.cb_Technologie.setValue(this.drucker.getTechnologie());
            this.cb_Format.setValue(this.drucker.getPapierFormatMax());
            this.cb_Raum.setValue(this.drucker.getRaum());
        } catch (Exception e) {

        }
    }

    public void error(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Fehler beim Speichern");
        alert.setContentText(message);

        alert.showAndWait();
    }
}