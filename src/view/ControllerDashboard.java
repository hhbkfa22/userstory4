package view;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;

public class ControllerDashboard
{
    @FXML
    private void handleBtnRechnerverwaltung(ActionEvent e)
    {
        ViewManager.getInstance()
                   .activateScene(ViewManager.getInstance()
                                             .getScene_rechner());
    }
    
    @FXML
    private void handleBtnDruckerverwaltung(ActionEvent e)
    {
        ViewManager.getInstance()
                   .activateScene(ViewManager.getInstance()
                                             .getScene_drucker());
    }

    @FXML
    private void handleBtnRaumverwaltung(ActionEvent e)
    {
        ViewManager.getInstance()
                .activateScene(ViewManager.getInstance()
                                            .getScene_raum());
    }
}
