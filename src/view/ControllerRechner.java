package view;

import Dao.DaoManager;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import Verwaltung.Raum;
import Verwaltung.Rechner;

import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

public class ControllerRechner implements Initializable
{
    public static int anzahl;
    @FXML
    private ListView<Rechner> lv_objects;

    private ObservableList<String> statusListe = FXCollections.observableArrayList("ok", "in Reperatur", "defekt");
    private ObservableList<String> typListe = FXCollections.observableArrayList(
            "Gaming-PC",
            "Multimedia-PC",
            "Office-PC"
    );
    private ObservableList<Rechner> rechnerListe = FXCollections.observableArrayList();
    private ObservableList<Raum> raumListe = FXCollections.observableArrayList();
    @FXML
    private TextField tf_id;
    @FXML
    private TextField tf_seriennummer;
    @FXML
    private TextField tf_modell;
    @FXML
    private ChoiceBox<String> cb_status;
    @FXML
    private ChoiceBox<Raum> cb_Raum;
    @FXML
    private TextField tf_hersteller;
    @FXML
    private TextField tf_garantie;
    @FXML
    private DatePicker dp_lieferdatum;
    @FXML
    private TextField tf_cpu;
    @FXML
    private TextField tf_ram;
    @FXML
    private TextField tf_os;
    @FXML
    private ChoiceBox<String> cb_typ;
    @FXML
    private TextField tf_gpu;
    @FXML
    private TextField tf_ssd;
    @FXML
    private TextField tf_hdd;

    private Rechner rechner;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle){
        var daoManager = DaoManager.getInstance();
        rechnerListe = daoManager.getListe_rechner();
        raumListe = daoManager.getListe_raum();
        anzahl+=rechnerListe.size();
        anzahl++;
        this.tf_id.setText(Integer.toString(anzahl));
        this.cb_status.setValue("ok");
        this.cb_status.setItems(this.statusListe);
        this.cb_typ.setValue("Gaming-PC");
        this.cb_typ.setItems(this.typListe);
        this.cb_Raum.setItems(raumListe);
        this.cb_Raum.setValue(raumListe.get(0));
        this.lv_objects.setItems(rechnerListe);
    }

    @FXML
    private void handleBtnDashboard(ActionEvent e)
    {
        ViewManager.getInstance()
                   .activateScene(ViewManager.getInstance()
                                             .getDashboardscene());
    }

    public void save() {
        try {
            String modell = this.tf_modell.getText();
            String seriennummer = this.tf_seriennummer.getText();
            String hersteller  = this.tf_hersteller.getText();
            String status = this.cb_status.getValue();
            int garantie = Integer.parseInt(this.tf_garantie.getText());
            LocalDate lieferdatum = this.dp_lieferdatum.getValue();
            String cpu = this.tf_cpu.getText();
            int ram = Integer.parseInt(this.tf_ram.getText());
            String os = this.tf_os.getText();
            String typ = this.cb_typ.getValue();
            String gpu = this.tf_gpu.getText();
            int ssd = Integer.parseInt(this.tf_ssd.getText());
            int hdd = Integer.parseInt(this.tf_hdd.getText());
            Raum raum = this.cb_Raum.getValue();

            if (this.rechner == null) {
                Rechner rechner = new Rechner(
                        seriennummer,
                        modell,
                        hersteller,
                        status,
                        garantie,
                        lieferdatum,
                        cpu,
                        ram,
                        os,
                        typ,
                        gpu,
                        ssd,
                        hdd,
                        raum);

                this.rechnerListe.add(rechner);
                anzahl++;
                this.tf_id.setText(Integer.toString(anzahl));
                this.reset();
            } else {
                this.rechner.setModell(modell);
                this.rechner.setSerienNummer(seriennummer);
                this.rechner.setHersteller(hersteller);
                this.rechner.setStatus(status);
                this.rechner.setGarantie(garantie);
                this.rechner.setLieferDatum(lieferdatum);
                this.rechner.setCpu(cpu);
                this.rechner.setRam(ram);
                this.rechner.setOs(os);
                this.rechner.setTyp(typ);
                this.rechner.setGpu(gpu);
                this.rechner.setSsd(ssd);
                this.rechner.setHdd(hdd);
                this.rechner.setRaum(raum);

                this.rechnerListe.set(lv_objects.getSelectionModel().getSelectedIndex(), null);
                this.rechnerListe.set(lv_objects.getSelectionModel().getSelectedIndex(), this.rechner);
                this.rechner = null;
            }
        } catch (NumberFormatException e) {
            error("Jedes Textfeld muss mit einem Wert gefüllt sein!");
        } catch (Exception e) {
            error("Ein Fehler beim Speichern ist aufgetreten. Versuchen Sie es bitte erneut!");
        }
    }

    public void reset() {
        this.tf_id.setText(Integer.toString(anzahl));
        this.tf_modell.setText("");
        this.tf_seriennummer.setText("");
        this.tf_hersteller.setText("");
        this.cb_status.setValue("ok");
        this.tf_garantie.setText("");
        this.dp_lieferdatum.setValue(null);
        this.tf_cpu.setText("");
        this.tf_ram.setText("");
        this.tf_os.setText("");
        this.cb_typ.setValue("Gaming-PC");
        this.tf_gpu.setText("");
        this.tf_ssd.setText("");
        this.tf_hdd.setText("");
    }

    public void loadRechner()
    {
        try {
            this.rechner = rechnerListe.get(lv_objects.getSelectionModel().getSelectedIndex());
            this.tf_id.setText(Integer.toString(this.rechner.getId()));
            this.tf_modell.setText(this.rechner.getModell());
            this.tf_seriennummer.setText(this.rechner.getSerienNummer());
            this.tf_hersteller.setText(this.rechner.getHersteller());
            this.cb_status.setValue(this.rechner.getStatus());
            this.tf_garantie.setText(Integer.toString(this.rechner.getGarantie()));
            this.dp_lieferdatum.setValue(this.rechner.getLieferDatum());
            this.tf_cpu.setText(this.rechner.getCpu());
            this.tf_ram.setText(Integer.toString(this.rechner.getRam()));
            this.tf_os.setText(this.rechner.getOs());
            this.cb_typ.setValue(this.rechner.getTyp());
            this.tf_gpu.setText(this.rechner.getGpu());
            this.tf_ssd.setText(Integer.toString(this.rechner.getSsd()));
            this.tf_hdd.setText(Integer.toString(this.rechner.getHdd()));
            this.cb_Raum.setValue(this.rechner.getRaum());
        } catch (Exception e) {

        }
    }

    public void error(String message) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Fehler beim Speichern");
        alert.setContentText(message);

        alert.showAndWait();
    }
}
